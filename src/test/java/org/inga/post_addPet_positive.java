package org.inga;

import org.json.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;
import static org.junit.Assert.*;
import java.net.URI;

public class post_addPet_positive {
    static String petBaseUrl = "";

    int returnedCode  = 0;

    @Given("I request post pet api endpoint")
    public void post_api(){
        petBaseUrl = "https://petstore.swagger.io/v2/pet";
    }

    @When("I specify parameters for a pet {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void enter_parameters (String id, String category_id, String category_name, String name, String photoUrls, String tag_id, String tag_name, String status) {

        JSONObject category = new JSONObject();
        category.put("id",category_id);
        category.put("name",category_name);

        JSONObject petdata = new JSONObject();
        petdata.put("id", id);
        petdata.put("name", name);

        JSONArray photos = new JSONArray();
        photos.put(photoUrls);

        petdata.put("photoUrls", photos);
        petdata.put("status", status);
        petdata.put("category", category);

        JSONObject tag = new JSONObject();
        tag.put("id",tag_id);
        tag.put("name", tag_name);

        JSONArray tags = new JSONArray();
        tags.put(tag);

        petdata.put("tags", tags);

        String jsonString = petdata.toString(2);

        SerenityRest.setDefaultBasePath(petBaseUrl);
        returnedCode = SerenityRest
                .given()
                    .contentType("application/json")
                    .body(jsonString)
                .when()
                    .post(URI.create(petBaseUrl))
                    .getStatusCode();
    }

    @Then("I get response code 200, the pet is added to the store")
    public void successful () {
        assertEquals(200,
                returnedCode);
    }

}