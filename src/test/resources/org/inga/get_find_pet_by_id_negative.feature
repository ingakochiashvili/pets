Feature: get_find_pet_by_id_negative

  Scenario Outline: Impossible to find pets by invalid id
    Given I request get petid ndpoint
    And pets exist in the system
    When I request pet data by invalid id "<find_invalid_id>"
    Then I get no pet
    And I get response code 400 (invalid id)

    Examples:
    | find_invalid_id |
    | -1233           |
    |                 |
    | text            |
    | *2345           |
    | 1234*           |
    | 547 8           |