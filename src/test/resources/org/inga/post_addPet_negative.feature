Feature: post_addPet_negative

  Background:
    Given I request post new pet api endpoint
    And pets id is an unique integer


  Scenario Outline: Adding a pet with incorrect id to the store is impossible
  Assumption: All of the parameters are mandatory
    Given a pet with id "<ids>" exists in the store
    When I create a pet with the incorrect id "<incorrect_id>"
    And I use correct values for other parameters: "<category_ids>", "<category_names>", "<names>", "<photoUrls>", "<tags_id>", "<tags_name>", "<statuses>"
    Then I get response code 405 (invalid input), the pet is not added to the store

    Examples:
      | ids    | incorrect_id | category_ids | category_names | names  | photoUrls         | tags_id | tags_name       | statuses  |
      | 37645  |  37645       | 5648888      | cat            | tom    | https://myurl.com | 987688  | white           | available |
      | 1345   |  *67543      | 7748988      | dog            | bibi   | https://myurl.com | 987331  | fun             | pending   |
      | 09054  | s4fd@!vv%    | 7685943      | bird           | ray    | https://myurl.com | 327232  | colorful parrot | sold      |
      | 342    | aa234        | 5454545      | bird           | koko   | https://myurl.com | 327232  | colorful parrot | sold      |
      | 02384  | -56743       | 6575433      | dog            | buba   | https://myurl.com | 546761  | brown grey      | pending   |
      | 32441  | 121 21       | 1575133      | hamster        | jiji   | https://myurl.com | 544766  |  yellow         | available |



  Scenario Outline: Adding a pet with empty values to the store is impossible
    When I create a pet with empty one of the parameters: "<e_id>", "<e_category_id>", "<e_category_name>", "<e_name>", "<e_photoUrls>", "<e_tag_id>", "<e_tag_name>", "<e_status>"
    Then I get response code 405 (invalid input), no pet is added to the store

    Examples:
      | e_id  | e_category_id | e_category_name | e_name | e_photoUrls       | e_tag_id | e_tag_name  | e_status  |
      |       | 4343434       | cats            | scotty | https://myurl.com | 1111     | black&white | available |
      | 1200  |               | birds           | coco   | https://myurl.com | 657653   | colorful    | sold      |
      | 88    | 564444        |                 | bobi   | https://myurl.com | 55       | colorful    | available |
      | 533   | 656565        | dog             |        | https://myurl.com | 657653   | colorful    | sold      |
      | 8876  | 44477         | ccc             | coco   |                   | 456      | red         | pending   |
      | 0456  | 564123        | nkn             | xxxx   | https://test.com  |          | colorful    | sold      |
      | 7654  | 453           | mouse           | coco   | https://test.com  | 6754     |             | available |
      | 56    | 6333          | ccc             | zizi   | https://test.com  | 9576     | green       |           |