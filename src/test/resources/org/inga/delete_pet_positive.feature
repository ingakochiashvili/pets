Feature: delete_pet_positive

  Scenario: Possible to delete pet by id
    Given pets exist to the store system
    And I request delete pet petid api endpoint
    When I specify id of a pet
    And I specify api key
    And I send the request
    Then I get response code 200, the pet is deleted from the system
