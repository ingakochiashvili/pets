Feature: delete_pet_negative

  Scenario Outline: Impossible to delete pet by incorrect id or api key
    Given pets exist to the store system
    And I request delete petId api endpoint
    When I specify incorrect values in id or apy key "<del_id>", "<api_key>"
    And I send the request
    Then I get response code 400, no pet is deleted

    Examples:
    | del_id    | api_key |
    |           | string  |
    | 3453      |         |
    | %$#5      | key     |
    | 543 33    | AAA     |
    | textid    | correct |


