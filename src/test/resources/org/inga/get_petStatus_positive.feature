Feature: get_petStatus_positive
  Background:
    Given I request get pet​ find by status endpoint
    And three valid statuses exist for pets in the system: available, pending, sold

  Scenario Outline: Possible to find pets by status
    Given "<pets_quantity>" pets have the status "<pet_status>"
    When I request pet data by status "<valid_status>"
    Then I get all pets with specified status
    And I get response code 200 (successful operation)

    Examples:
      | valid_status | pet_status | pets_quantity |
      | available    | available  | 3             |
      | pending      | pending    | 10            |
      | sold         | sold       | 0             |

  Scenario: Possible to find pets by multiple status
    When I request pets data by multiple statuses available, pending, sold
    Then I get all pets with specified statuses
    And I get response code 200 (successful operation)

