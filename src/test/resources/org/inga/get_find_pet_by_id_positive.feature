Feature: get_find_pet_by_id_positive

  Scenario: Possible to find pets by id
    Given I request get pet petid endpoint
    And pets exist in the system
    When I request pet data by id
    Then I get a pet with specified id
    And I get response code 200 (successful operation)