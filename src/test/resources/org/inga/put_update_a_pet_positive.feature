Feature: put_update_a_pet_positive

  Scenario Outline: Existed pet data can be updated
    Given pets exist to the store system
    And I request put pet api endpoint
    When I specify the existed pet "<pet_ids>"
    And I specify new values for other parameters: "<pet_category_ids>", "<pet_category_names>", "<pet_names>", "<pet_photoUrls>", "<pet_tag_ids>", "<pet_tag_names>", "<pet_statuses>"
    And I send the request
    Then I get response code 200, the pet data is updated

    Examples:
      | pet_ids | pet_category_ids | pet_category_names | pet_names | pet_photoUrls         | pet_tag_ids | pet_tag_names | pet_statuses |
      | 12321   | 45337            | dog                | busa      | https://simpleurl.com | 54444       | red           | pending      |
      | 567     | 54368            | cat                | tom       | https://simpleurl.com | 77          | white         | sold         |