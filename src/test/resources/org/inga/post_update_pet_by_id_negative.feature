Feature: post_update_pet_by_id_negative

  Scenario Outline: Impossible to update pet by incorrect parameters
    Given pets exist to the store system
    And I request post petid api endpoint
    When I specify incorrect parameters: "<petids>","<pet_names_inv>", "<pet_statuses_inv>"
    And I send the request
    Then I get response code 400, no pet is updated

    Examples:
      | petids | pet_names_inv | pet_statuses_inv |
      | @@@654 | doggy         | available        |
      |        | scotty        | sold             |
      | TextID | coco          | pending          |
      | 7654   |               | sold             |
      | 5433   | baxy          |                  |
      | 5433   | baxy          | new              |