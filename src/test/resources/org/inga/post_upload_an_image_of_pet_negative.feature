Feature: post_upload_an_image_of_pet_negative

  Background:
    Given pets exist to the store system
    And I request post petid upload mage api endpoint

  Scenario Outline: Uploading an image of pet without id is impossible
    When I specify an image for a pet
    And I specify invalid id "<invalid_id>"
    And I send the request
    Then I get response code 405, no image is uploaded for the pet

   Examples:
     | invalid_id |
     |            |
     | @#$ vbn )) |
     | ab4564     |
     | 11564n     |
     | 56 7868    |
     | 456*       |
     | %986       |
     | -9879      |



  Scenario: Upload a non-image file for a pet is impossible
    When I specify a non-image file for a pet
    And I specify a pet id
    And I send the request
    Then I get response code 405, no image is uploaded for the pet