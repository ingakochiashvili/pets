Feature: put_update_a_pet_negative

  Scenario Outline: No pet can be updated when id is incorrect
    And I request put pet api endpoints
    When I specify such id "<selected_id>" that doesn't match to any real id "<real_id>"
    And I specify values for other parameters: "<pet_category_id>", "<pet_category_name>", "<pet_name>", "<pet_photoUrl>", "<pet_tag_id>", "<pet_tag_name>", "<pet_st>"
    And I send the request
    Then I get response code 400, no pet is updated

    Examples:
      | real_id      | selected_id |pet_category_id | pet_category_name | pet_name | pet_photoUrl          | pet_tag_id | pet_tag_name | pet_st  |
      | 12321        | 564*        | 45337          | dog               | busa     | https://simpleurl.com | 54444      | red          | pending |
      | 54377        | @@345       | 45337          | dog               | busa     | https://simpleurl.com | 54444      | red          | pending |
      | 62321        | 12 321      | 45337          | dog               | busa     | https://simpleurl.com | 54444      | red          | pending |
      | 876          |             | 45337          | dog               | busa     | https://simpleurl.com | 54444      | red          | pending |
      | 6654         | Dfgh        | 45337          | dog               | busa     | https://simpleurl.com | 54444      | red          | pending |