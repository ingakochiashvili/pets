Feature: get_find_pet_by_tags_positive

  Background:
    Given I request get pet findbytags endpoint
    And pets exist in the system

  Scenario Outline: Possible to find pets by tags (tag id and tag name)
    When I request pet data by tag "<tag_id>" "<tag_name>"
    Then I get a pet with specified tag
    And I get response code 200 (successful operation)

    Examples:
      | tag_id | tag_name  |
      | 354    | name_text |
      | 15     | beauty    |


  Scenario Outline: Possible to find pets by multiple tags
    When I request multiple pets data by tags: "<tag1_id>" "<tag1_name>", "<tag2_id>" "<tag2_name>", "<tag3_id>" "<tag3_name>"
    Then I get all pets with specified tags
    And I get response code 200 (successful operation)

    Examples:
      | tag1_id | tag1_name | tag2_id | tag2_name| tag3_id | tag3_name |
      | 353456  | name1     | 54365   | name2    | 654     | name3     |

