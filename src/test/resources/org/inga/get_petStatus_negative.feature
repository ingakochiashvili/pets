Feature: get_petStatus_negative

  Scenario Outline: Impossible to find pets with invalid status
    Given I request get pet​ by status endpoint
    And three valid statuses exist for pets in the system: available, pending, sold
    When I request pet data by invalid status "<invalid_value>"
    Then I get no pets
    And I get response code 400 (invalid status value)

    Examples:
      | invalid_value    |
      | some text status |
      |                  |
      | AvAiLAble        |
      | 2347             |
      | *&N_             |