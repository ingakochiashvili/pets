Feature: post_update_pet_by_id_positive

  Scenario Outline: Possible to update pet by id using form data
    Given pets exist to the store system
    And I request post pet petid api endpoint
    When I specify a pet "<petid>"
    And I specify new values for parameters: "<pets_name>", "<pets_status>"
    And I send the request
    Then I get response code 200, the pet data is updated

    Examples:
      | petid | pets_name | pets_status |
      | 1232  | doggy     | available   |
      | 567   | scotty    | sold        |
      | 99    | coco      | pending     |