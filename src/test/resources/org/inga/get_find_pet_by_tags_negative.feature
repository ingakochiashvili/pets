Feature: get_find_pet_by_tags_negative

  Scenario Outline: Impossible to find pets by incorrect tags (tag id and tag name)
    Given I request get pet bytags endpoint
    And pets exist in the system
    When I request pet data by a tag that doesn't match to any existed pet's tag "<incorrect_tag_id>", "<incorrect_tag_name>"
    Then I get no pet
    And I get response code 400 (invalid tag value)

    Examples:
      | incorrect_tag_id | incorrect_tag_name |
      | 15               |                    |
      |                  | name_text          |
      | @@234            |                    |
      | text             |  any               |
      | -45 33           | xxx                |
      | 123*             |  nnn               |



