Feature: post_addPet_positive

  Scenario Outline: Add a new pet to the store
    Given I request post pet api endpoint
    When I specify parameters for a pet "<id>", "<category_id>", "<category_name>", "<name>", "<photoUrl>", "<tag_id>", "<tag_name>", "<status>"
    Then I get response code 200, the pet is added to the store

    Examples:
      | id    | category_id | category_name | name   | photoUrl          | tag_id | tag_name    | status    |
      | 12321 | 4343434     | cats          | scotty | https://myurl.com | 656546 | black&white | available |
      | 0     | 1           | birds         | coco   | https://myurl.com | 657653 | colorful    | sold      |