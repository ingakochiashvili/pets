Feature: post_upload_an_image_of_pet_positive

  Background:
    Given pets exist to the store system
    And I request post pet petid upload image api endpoint

  Scenario: Upload an image of a pet with additional metadata
    When I specify an image, id, additional metadata of a pet
    And I send the request
    Then I get response code 200, the image is uploaded for the pet.


  Scenario: Upload an image of pet without additional metadata
    When I specify an image, pet id
    And I do not specify additional metadata
    And I send the request
    Then I get response code 200, the image is uploaded for the pet.


  Scenario: Upload a new image of pet
    Given images already exist for a pet
    When I specify a new image, pet id, metadata
    And I send the request
    Then I get response code 200, the new image is uploaded for selected pet, old images are also present.

